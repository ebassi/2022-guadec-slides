% Amberol: A music player with no delusions of grandeur
% Emmanuele Bassi
% GUADEC 2022

## How to learn a new framework

* or something close to that
* know Rust (somewhat)
* know GTK (somewhat)
* know GNOME (somewhat)

----

## Rust and GUI development

* Rust and GTK
* two great taste that taste great together
* no need to use Electron
* no need to wait until somebody writes a whole toolkit in Rust
* why would you want that anyway?

----

## Use what Rust provides

* Crates, lots of crates
* Type safety
* Good coding practices
* Static analysis and code helpers
* Examples, documentation

----

## Use what GNOME provides

* Meson
* Flatpak
* GNOME Builder
* Examples, documentation
* Other Rust/GTK applications

----

## Amberol

![](./images/amberol-1.png)

----

## Plays songs…

![](./images/amberol-2.png)

----

## … Looks good while doing it

![](./images/amberol-3.png)

----

## How simple can you get?

> A complex system that works is invariably found to have evolved from a simple system that works. A complex system designed from scratch never works and cannot be patched up to make it work. You have to start over with a working simple system.
> 
> — Gall's Law, *Systemantics*

----

## Simple is good

* Good learning opportunity
* Build incrementally
* Design iteratively

----

## Resources

* [Amberol](https://apps.gnome.org/app/io.bassi.Amberol/)
* [Repository](https://gitlab.gnome.org/World/amberol)
* [Flathub](https://flathub.org/apps/details/io.bassi.Amberol)
* [Twitch](https://twitch.tv/ebassi)
